var CALENDARS = ['Családi naptár', 'Contacts'];

function doGet() {
    Logger.log('begin');
    var events = getEventsForDay(new Date());
    var newEvents = [];
    events.forEach(function(ev) {
        addIfEnabled(ev, newEvents)
    });

    Logger.log('end - ' + newEvents);
    var json = JSON.stringify({events: newEvents});
    var textOutput = ContentService.createTextOutput(json);
    textOutput.setMimeType(ContentService.MimeType.JSON);
    return textOutput;
}

function getEventsForDay(date) {
    var calendars = CalendarApp.getAllCalendars();
    var allEvents = [];
    calendars.filter(isImportantCalendar)
        .forEach(function (elem) {
            var events = elem.getEventsForDay(date);
            allEvents = allEvents.concat(events);
            Logger.log(elem.getName() + ' - ' + events);
        });
    return allEvents;
}

function isImportantCalendar(calendar) {
    return CALENDARS.indexOf(calendar.getName()) >= 0 || calendar.isMyPrimaryCalendar();
}

function addIfEnabled(event, newEvents) {
    var title = event.getTitle();
    Logger.log('Event: - ' + title);
    if (event.getDescription().indexOf('#') < 0) {
        newEvents.push(event.getTitle());
    }
}

